package ca.intelliware

import arrow.core.Either
import arrow.core.Either.Left
import arrow.core.Either.Right
import java.math.BigDecimal

fun main(args: Array<String>) {
    println("Try to create Widget X1234")
    println(WidgetCode.create("X1234"))
    println("Now try to create Widget W1234")
    val w = WidgetCode.create("W1234")
    println(w)
    when (w) {
        is Left -> println("Still invalid")
        is Right -> println(w.b)
    }

    val g = GizmoCode.create("g123")
    when (g) {
        is Left -> println("Still invalid")
        is Right -> println(g.b)
    }

    val uq: Either<String, OrderQuantity> = UnitQuantity.create(1)
    when (uq) {
        is Left -> println("woops $uq.a")
        is Right -> when (val validQuantity = uq.b) {
            is UnitQuantity -> println(validQuantity.quantity)
            is KilogramQuantity -> println(validQuantity.quantity)
        }
    }


    val oqk: Either<String, OrderQuantity> = KilogramQuantity.create(BigDecimal("1.0"))
    when (oqk) {
        is Left -> println("woops $oqk.a")
        is Right -> when (val validQuantity = oqk.b) {
            is UnitQuantity -> println(validQuantity.quantity)
            is KilogramQuantity -> println(validQuantity.quantity)
        }
    }

    val orderResult = placeOrder(UnvalidatedOrder())
}

