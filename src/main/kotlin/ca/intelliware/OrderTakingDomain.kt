package ca.intelliware

import arrow.core.*
import java.math.BigDecimal

// temporarily abstract until we provide an implementation
//abstract class PlaceOrder : (UnvalidatedOrder) -> Either<PlaceOrderError, PlaceOrderEvents>
typealias PlaceOrder = (UnvalidatedOrder) -> Either<PlaceOrderError, PlaceOrderEvents>

val placeOrder: PlaceOrder = { unvalidatedOrder: UnvalidatedOrder ->
    Either.Left(ValidationErrors(listOf(ValidationError("someField", "someErrorDescription"))))
}

typealias UnvalidatedOrder = NotImplementedError


data class PlaceOrderEvents(
    val acknowledgementSent: AcknowledgementSentEvent,
    val orderPlaced: OrderPlacedEvent,
    val billableOrderPlaced: BillableOrderPlacedEvent
)

typealias AcknowledgementSentEvent = NotImplementedError
typealias OrderPlacedEvent = NotImplementedError
typealias BillableOrderPlacedEvent = NotImplementedError

sealed class PlaceOrderError
data class ValidationErrors(val errors: List<ValidationError>) : PlaceOrderError()
data class ValidationError(val fieldName: String, val errorDescription: String)


data class Order(
    val id: OrderId,
    val customerId: CustomerId,
    val shippingAddress: ShippingAddress,
    val billingAddress: BillingAddress,
    val orderLines: NonEmptyList<OrderLine>,
    val amountToBill: BillingAmount
)

data class OrderLine(
    val id: OrderLineId,
    val orderId: OrderId,
    val productCode: ProductCode,
    val orderQuantity: OrderQuantity,
    val price: Price
)

typealias OrderId = NotImplementedError
typealias OrderLineId = NotImplementedError
typealias CustomerId = NotImplementedError

typealias CustomerInfo = NotImplementedError
typealias BillingAddress = NotImplementedError
typealias ShippingAddress = NotImplementedError

inline class Price(val value: BigDecimal)
inline class BillingAmount(val value: BigDecimal)


sealed class ProductCode
data class WidgetCode private constructor(val code: String) : ProductCode() {
    companion object {
        fun create(_code: String): Either<IllegalArgumentException, WidgetCode> {
            return if (_code.matches("[Ww]\\d\\d\\d\\d".toRegex()))
                Right(WidgetCode(_code.toUpperCase()))
            else Left(
                IllegalArgumentException("Widget code was $_code but must be W9999")
            )
        }
    }
}

data class GizmoCode private constructor(val code: String) : ProductCode() {
    companion object {
        fun create(_code: String): Either<IllegalArgumentException, GizmoCode> {
            return if (_code.matches("[Gg]\\d\\d\\d".toRegex()))
                Right(GizmoCode(_code.toUpperCase()))
            else Left(
                IllegalArgumentException("Gizmo code was $_code but must be G999")
            )
        }
    }
}

sealed class OrderQuantity
data class UnitQuantity private constructor(val quantity: Int) : OrderQuantity() {
    companion object {
        fun create(quantity: Int): Either<String, UnitQuantity> {
            return when {
                quantity < 0 -> Either.Left("UnitQuantity cannot be negative")
                quantity > 1000 -> Either.Left("UnitQuantity cannot be greater than 1000")
                else -> Either.Right(UnitQuantity(quantity))
            }
        }
    }
}

data class KilogramQuantity private constructor(val quantity: BigDecimal) : OrderQuantity() {
    companion object {
        fun create(quantity: BigDecimal): Either<String, KilogramQuantity> {
            return when {
                quantity < BigDecimal(0.05) -> Either.Left("KilogramQuantity cannot be less than 0.05 kg")
                quantity > BigDecimal(100) -> Either.Left("KilogramQuantity cannot be greater than 100 kg")
                else -> Either.Right(KilogramQuantity(quantity))
            }
        }
    }
}

sealed class CustomerEmail
data class UnverifiedEmailAddress(val emailAddress: EmailAddress) : CustomerEmail()
data class VerifiedEmailAddress private constructor(val emailAddress: EmailAddress) : CustomerEmail()

data class EmailAddress private constructor(val address: String) {
    companion object {
        fun create(_address: String): Either<String, EmailAddress> {
            return when {
                isValidEmail(_address) -> Either.Right(EmailAddress(_address))
                else -> Either.Left("Invalid email address")
            }
        }
    }
}

fun isValidEmail(address: String): Boolean {
    // TODO: regex stuff
    return true
}

// Can only send a password reset email to a verfied address
typealias SendPasswordResetEmail = (VerifiedEmailAddress) -> Unit


typealias AddressValidationService = (UnvalidatedAddress) -> Option<ValidatedAddress>
typealias UnvalidatedAddress = NotImplementedError
typealias ValidatedAddress = NotImplementedError

typealias ChangeOrderLinePrice = (order: Order, orderLineId: OrderLineId, newPrice: Price) -> Order

val changeOrderLinePrice: ChangeOrderLinePrice = { order: Order, orderLineId: OrderLineId, newPrice: Price ->
    val newOrderLines: NonEmptyList<OrderLine> = order.orderLines.map { ol ->
        if (ol.id == orderLineId) ol.copy(price = newPrice) else ol
    }
    val newAmountToBill: BillingAmount = newOrderLines
        .map { it.price }
        .foldLeft(BillingAmount(BigDecimal.ZERO)) { acc, n -> BillingAmount(acc.value.add(n.value)) }
    order.copy(orderLines = newOrderLines, amountToBill = newAmountToBill)
}
